import datetime
import json
import os
from urllib.request import urlopen
from urllib.parse import quote
from urllib.error import HTTPError
import feedparser
from flask import Flask
from flask import make_response
from flask import render_template
from flask import request

app = Flask(__name__)

RSS_FEEDS = {
    'bbc': 'http://feeds.bbci.co.uk/news/rss.xml',
    'cnn': 'http://rss.cnn.com/rss/edition.rss',
    'fox': 'http://feeds.foxnews.com/foxnews/latest',
    'iol':  'http://www.iol.co.za/cmlink/1.640',
    }

DEFAULTS = {
    'publication': 'bbc',
    'city': 'London,UK',
    'currency_from': 'GBP',
    'currency_to': 'USD',
    }

WEATHER_URL = 'http://api.openweathermap.org/data/2.5/weather' + \
              '?q={}&units=metric&appid=' + os.getenv('WEATHER_API_KEY')
CURRENCY_URL = 'https://openexchangerates.org/api/latest.json?app_id=' + \
               os.getenv('CURRENCY_API_KEY')

def get_weather(query):
    query = quote(query)
    url = WEATHER_URL.format(query)

    try:
        data = urlopen(url).read()
    except HTTPError as e:
        print(e)
        return get_weather(DEFAULTS['city'])

    data = data.decode('utf-8')
    parsed = json.loads(data)
    weather = None
    if parsed.get('weather'):
        weather = {
            'description': parsed['weather'][0]['description'],
            'temperature': parsed['main']['temp'],
            'city': parsed['name'],
            'country': parsed['sys']['country'],
        }
    return weather


def get_news(publication):
    if not publication or publication.lower() not in RSS_FEEDS:
        publication = DEFAULTS['publication']
    else:
        publication = publication.lower()
    feed = feedparser.parse(RSS_FEEDS[publication])
    return feed['entries']


def get_rates(frm, to):
    try:
        all_currency = urlopen(CURRENCY_URL).read()
        all_currency = all_currency.decode('utf-8')
    except HTTPError as e:
        print(e)
        return 0

    parsed = json.loads(all_currency).get('rates')
    frm_rate = parsed.get(frm.upper())
    to_rate = parsed.get(to.upper())
    return (to_rate / frm_rate, parsed.keys())


def get_value_with_fallback(key):
    if request.args.get(key):
        return request.args.get(key)
    if request.cookies.get(key):
        return request.cookies.get(key)
    return DEFAULTS[key]


@app.route('/')
def home():
    publication = get_value_with_fallback('publication')
    articles = get_news(publication)

    city = get_value_with_fallback('city')
    weather = get_weather(city)

    currency_from = get_value_with_fallback('currency_from')
    currency_to = get_value_with_fallback('currency_to')
    rate, currencies = get_rates(currency_from, currency_to)

    response = make_response(render_template('home.html',
                                             articles=articles,
                                             weather=weather,
                                             currency_from=currency_from,
                                             currency_to=currency_to,
                                             rate=rate,
                                             currencies=sorted(currencies)))

    expires = datetime.datetime.now() + datetime.timedelta(days=365)
    response.set_cookie('publication', publication, expires=expires)
    response.set_cookie('city', city, expires=expires)
    response.set_cookie('currency_from', currency_from, expires=expires)
    response.set_cookie('currency_to', currency_to, expires=expires)
    return response


if __name__ == '__main__':
    app.run(port=5000, debug=True)
